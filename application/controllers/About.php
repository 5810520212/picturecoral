<?php
/**
 * Created by PhpStorm.
 * UserModel: Wassana-lerdna
 * Date: 22/12/2561
 * Time: 14:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->load->view('about');
    }
}