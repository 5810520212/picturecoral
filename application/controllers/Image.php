<?php
/**
 * Created by PhpStorm.
 * UserModel: Wassana-lerdna
 * Date: 22/12/2561
 * Time: 14:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        redirect("Main", 'refresh');
    }

    public function download($id_image) {
        $this->load->model('image_model');
        $image = $this->image_model->get_image($id_image);

        if ($image === FALSE) {
            redirect("Main", 'refresh');
        }

        $data['url'] = './' . $image['path_image'];
        $this->load->view('download', $data);
    }

    public function show_image($id_image) {
        $data['page'] = "image";

        $this->load->model('image_model');

        $image = $this->image_model->get_image($id_image);
        if ($image === FALSE) {
            $this->load->library('user_agent');
            $referrer =  $this->agent->referrer();
            redirect($referrer, 'refresh');
        }

        $this->load->model('user_model');
        $user = $this->user_model->get_user($image['id_user']);

        $this->load->model('image_tag_model');
        $tag = $this->image_tag_model->get_by_image($id_image);

        $this->load->model('tag_model');
        $name_tag = array();
        if ($tag !== FALSE) {
            foreach ($tag as $t) {
                $tag_form_model = $this->tag_model->get_tag_by_idtag($t['id_tag']);
                if ($t['id_tag'] == $tag_form_model['id']) {
                    $name_tag[] = $tag_form_model['name'];
                }
            }
        }

        $this->load->model('favorite_model');

        $data['tag'] = $name_tag;
        $data['owner'] = $user;
        $data['image'] = $image;

        $user = $this->user_model->get_user_by_name($this->session->set_session_name);
        $data['is_favorite'] = $this->favorite_model->get($id_image, $user['id']);
        $this->load->view('header', $data);
        $this->load->view('content_image', $data);
    }

    public function favorite_submit($id_image) {

        check_login();

        $this->load->model('user_model');
        $this->load->model('favorite_model');

        $this->load->library('user_agent');
        $referrer =  $this->agent->referrer();

        $user = $this->user_model->get_user_by_name($this->session->set_session_name);

        $favorite = $this->favorite_model->get($id_image, $user['id']);

        if ($favorite !== FALSE) {
            $this->favorite_model->delete($id_image, $user['id']);
        } else {
            $submit_data['id_image'] = $id_image;
            $submit_data['id_user'] = $user['id'];
            $this->favorite_model->insert_data($submit_data);
        }

        redirect($referrer, 'refresh');

    }

    public function delete_submit($id_image) {

        check_login();

        $this->load->model('user_model');
        $this->load->model('favorite_model');
        $this->load->model('image_model');
        $this->load->model('image_tag_model');

        $this->load->library('user_agent');
        $referrer =  $this->agent->referrer();

        $user = $this->user_model->get_user_by_name($this->session->set_session_name);
        $image = $this->image_model->get_image($id_image);

        if ($image === FALSE) {
            $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            redirect($referrer, 'refresh');
        }
        if ($image['id_user'] !== $user['id']) {
            $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            redirect($referrer, 'refresh');
        }

        $this->favorite_model->delete($image['id'], $user['id']);
        $this->image_tag_model->delete_by_image($image['id']);
        $this->image_model->delete_image($image['id']);

        redirect($referrer, 'refresh');
    }
}