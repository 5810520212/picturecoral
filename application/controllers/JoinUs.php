<?php
/**
 * Created by PhpStorm.
 * UserModel: Wassana-lerdna
 * Date: 22/12/2561
 * Time: 14:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class JoinUs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        check_guest();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[20]|is_unique[user.name]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passwordconf]|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('passwordconf', 'Password Confirmation', 'trim|required');

        $this->load->library('user_agent');
        $referrer =  $this->agent->referrer();
        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('popup_show', 'popup_signup');
            $this->session->set_flashdata('error', validation_errors('<div class="error">', '</div>'));
            $this->session->set_flashdata('name', $this->input->post('name', TRUE));
            $this->session->set_flashdata('email', $this->input->post('email', TRUE));

        } else {
            $this->load->model('User_model');

            $submit_data['name'] = $this->input->post('name', TRUE);
            $submit_data['email'] = $this->input->post('email', TRUE);

            $password = $this->input->post('password', TRUE);
            $submit_data['password'] = password_hash($password, PASSWORD_BCRYPT);
            $submit_data['path_image'] = "public/images/user/default_profile_user.png";

            if ($this->User_model->insert_data($submit_data)) {
                $this->session->set_flashdata('notify_message', 'Add new user successfully.');
                $this->session->set_userdata('set_session_name', $submit_data['name']);
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }

        }
        redirect($referrer, 'refresh');
    }


}