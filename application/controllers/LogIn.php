<?php
/**
 * Created by PhpStorm.
 * UserModel: Wassana-lerdna
 * Date: 22/12/2561
 * Time: 14:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class LogIn extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        check_guest();

        $this->load->library('user_agent');
        $referrer =  $this->agent->referrer();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[20]|callback_check_database');

        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('popup_show', 'popup_login');
            $this->session->set_flashdata('error_login', validation_errors('<div class="error">', '</div>'));
            $this->session->set_flashdata('name', $this->input->post('name', TRUE));
            $this->session->set_flashdata('email', $this->input->post('email', TRUE));
            redirect($referrer, 'refresh');
        } else {
            redirect($referrer, 'refresh');
        }
    }

    public function check_database($password)
    {
        if (empty($password)) {
            $this->form_validation->set_message('check_database', 'The Password field is required.');
            return FALSE;
        }

        $this->load->model('User_model');

        $username = $this->input->post('email');

        $set_session_name = $this->User_model->login($username,$password);
        if ($set_session_name)
        {
            $this->session->set_userdata('set_session_name', $set_session_name);
            return TRUE;
        }
        $this->form_validation->set_message('check_database', 'Invalid username or password.');
        return FALSE;
    }

}