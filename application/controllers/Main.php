<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
//        $this->load->model('user');

    }

    public function index()
    {

        $this->load->model('user_model');
        $this->load->model('image_model');

        $this->load->library('pagination');

        $limit_per_page = 16;
        $start_index = $this->uri->segment(3) ? $this->uri->segment(3) : 0 ;
        $total_records = $this->image_model->count_image();

        $data['images'] = array();
        $data['pagination_link'] = "";

        if ($total_records > 0) {
            $data['images'] = $this->image_model->show_post_pagination($limit_per_page, $start_index);

            if ($data['images'] === FALSE) {
                $data['images'] = [];
            }

            $is_favorite_arr = [];

            if ($this->session->set_session_name) {
                $this->load->model('favorite_model');
                $user_id = $this->user_model->get_user_by_name($this->session->set_session_name)['id'];
                foreach ($data['images'] as $image) {
                    $is_favorite_arr[] = $this->favorite_model->get($image['id'], $user_id);
                }
            }

            $data['is_favorite_arr'] = $is_favorite_arr;

            $config['base_url'] = base_url('main/index/');
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $this->pagination->initialize($config);
            $data['pagination_link'] = $this->pagination->create_links();
        }

        $data['page'] = "main";
        $this->load->view('header', $data);
        $this->load->view('content', $data);
    }

    public function collection($collection)
    {
        $this->load->model('user_model');
        $this->load->model('image_model');
        $this->load->model('collection_model');

       $collection_data = $this->collection_model->get($collection);
       if ($collection_data === FALSE ){
           redirect("main", 'refreash');
       }

        $this->load->library('pagination');

        $limit_per_page = 16;
        $start_index = $this->uri->segment(4) ? $this->uri->segment(4) : 0 ;
        $total_records = $this->image_model->count_image(['id_collection' => $collection_data['id']]);

        $data['images'] = array();
        $data['pagination_link'] = "";

        if ($total_records > 0) {
            $data['images'] = $this->image_model->show_post_pagination($limit_per_page, $start_index, ['id_collection' => $collection_data['id']]);

            if ($data['images'] === FALSE) {
                $data['images'] = [];
            }

            $is_favorite_arr = [];

            if ($this->session->set_session_name) {
                $this->load->model('favorite_model');
                $user_id = $this->user_model->get_user_by_name($this->session->set_session_name)['id'];
                foreach ($data['images'] as $image) {
                    $is_favorite_arr[] = $this->favorite_model->get($image['id'], $user_id);
                }
            }

            $data['is_favorite_arr'] = $is_favorite_arr;

            $config['base_url'] = base_url('main/collection/'. $collection .'/');
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $this->pagination->initialize($config);
            $data['pagination_link'] = $this->pagination->create_links();
        }

        $data['page'] = "main";
        $this->load->view('header', $data);
        $this->load->view('content', $data);
    }

}