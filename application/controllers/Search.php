<?php
/**
 * Created by PhpStorm.
 * User: Wassana-lerdna
 * Date: 27/12/2561
 * Time: 13:54
 */

class Search extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index () {

        $this->load->model('image_model');
        $this->load->model('collection_model');
        $this->load->model('image_tag_model');
        $this->load->model('tag_model');
        $this->load->model('user_model');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('search', 'Search', 'trim');

        $this->load->library('pagination');

        $limit_per_page = 16;
        $start_index = $this->uri->segment(3) ? $this->uri->segment(3) : 0 ;

        $search = $this->input->get('search', TRUE);

        $data['page'] = "search";
        $data['images'] = array();
        $data['search'] = $search;
        $this->session->set_flashdata('search', $search);
        $data['pagination_link'] = "";

        if ($search == "") {
            $total_records = $this->image_model->count_image();
            if ($total_records > 0) {
                $data['images'] = $this->image_model->show_post_pagination($limit_per_page, $start_index);

                if ($data['images'] === FALSE) {
                    $data['images'] = [];
                }

                $is_favorite_arr = [];

                if ($this->session->set_session_name) {
                    $this->load->model('favorite_model');
                    $user_id = $this->user_model->get_user_by_name($this->session->set_session_name)['id'];
                    foreach ($data['images'] as $image) {
                        $is_favorite_arr[] = $this->favorite_model->get($image['id'], $user_id);
                    }
                }

                $data['is_favorite_arr'] = $is_favorite_arr;

                $config['base_url'] = base_url('search/index/');
                $config['reuse_query_string'] = TRUE;
                $config['total_rows'] = $total_records;
                $config['per_page'] = $limit_per_page;
                $this->pagination->initialize($config);
                $data['pagination_link'] = $this->pagination->create_links();
            }
        } else {
            $collections = $this->collection_model->get_bylike($search);
            $tag = $this->tag_model->get_bylike($search);

            if ($collections === FALSE) {
                $collections = [];
            }

            if ($tag === FALSE) {
                $tag = [];
            }

            $count = 0;
            foreach ($collections as $collection) {
                $count += $this->image_model->count_image(['id_collection' => $collection['id']]);
                if ($count > 0) {
                    break;
                }
            }

            if ($count == 0) {
                foreach ($tag as $t) {
                    $count += $this->image_tag_model->count_image(['id_tag' => $t['id']]);
                    if ($count > 0) {
                        break;
                    }
                }
            }

            $exist_records = $count;

            if ($exist_records > 0) {
                $id_array = array();
                foreach ($tag as $t) {
                    $imagetags = $this->image_tag_model->get_by_tag($t['id']);
                    if ($imagetags !== FALSE) {
                        foreach ($imagetags as $imagetag) {
                            if (!in_array($imagetag['id_image'], $id_array)){
                                $id_array[] = $imagetag['id_image'];
                            }
                        }
                    }
                }
                $data['images'] = $this->image_model->show_search_image_pagination($limit_per_page, $start_index, $collections, $id_array);

                if ($data['images'] === FALSE) {
                    $data['images'] = [];
                }

                $is_favorite_arr = [];

                if ($this->session->set_session_name) {
                    $this->load->model('favorite_model');
                    $user_id = $this->user_model->get_user_by_name($this->session->set_session_name)['id'];
                    foreach ($data['images'] as $image) {
                        $is_favorite_arr[] = $this->favorite_model->get($image['id'], $user_id);
                    }
                }

                $data['is_favorite_arr'] = $is_favorite_arr;

                $config['base_url'] = base_url('search/index/');
                $config['reuse_query_string'] = TRUE;
                $config['total_rows'] = $this->image_model->count_search_image_pagination($collections, $id_array);
                $config['per_page'] = $limit_per_page;
                $this->pagination->initialize($config);
                $data['pagination_link'] = $this->pagination->create_links();
            }

        }
        $this->load->view('header', $data);
        $this->load->view('content', $data);
    }
}