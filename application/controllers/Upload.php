<?php
/**
 * Created by PhpStorm.
 * UserModel: Wassana-lerdna
 * Date: 22/12/2561
 * Time: 14:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        check_login();

        $data['page'] = "upload";
        $this->session->set_flashdata('error_upload', '<p>&nbsp;</p>');
        $this->session->set_flashdata('error_formupload', '');
        $this->load->view('header', $data);
        $this->load->view('content_upload', $data);
    }

    public function after_upload()
    {
        check_login();

        $data['page'] = "upload";

        $this->load->library('form_validation');
        $this->form_validation->set_rules('tag[]', 'Tag', 'trim|callback_check_tag');
        $this->form_validation->set_rules('collections', 'Collections', 'trim|required|callback_check_collection');

        $do_upload = $this->do_upload();
        if($this->form_validation->run() == FALSE || $do_upload['result'] == FALSE){
            if (array_key_exists('error', $do_upload)){
                $this->session->set_flashdata('error_upload', $do_upload['error']);
            }
            $this->session->set_flashdata('error_formupload', validation_errors('<div class="error">', '</div>'));
            $this->load->view('header', $data);
            $this->load->view('content_upload', $data);
        } else {
            $this->load->model('image_model');
            $this->load->model('tag_model');
            $this->load->model('collection_model');
            $this->load->model('image_tag_model');
            $this->load->model('user_model');

            $collections = $this->input->post('collections', TRUE);
            $submit_data['id_collection'] = $this->collection_model->get($collections)['id'];

            $submit_data['path_image'] = $do_upload['data'];

            $name_user = $this->session->set_session_name;
            $id_user = $this->user_model->get_user_by_name($name_user)['id'];
            $submit_data['id_user'] = $id_user;

            $id_image = $this->image_model->insert_data($submit_data);
            if ($id_image) {
                $tag = $this->input->post('tag[]', TRUE);
                foreach ($tag as $t) {
                    $submit_image_tag['id_image'] = $id_image;
                    $id_tag = $this->tag_model->get($t)['id'];
                    $submit_image_tag['id_tag'] = $id_tag;
                    $this->image_tag_model->insert_data($submit_image_tag);
                }
                $this->session->set_flashdata('notify_message', 'Add new image successfully.');
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }
            redirect('/User', 'refresh');
        }
    }

    public function check_tag($tag)
    {
        $tag = $this->input->post('tag', TRUE);
        if ($tag == null){
            return TRUE;
        }
        if (!is_array($tag)) {
            $this->form_validation->set_message('check_tag', 'The Tag array field is invalid');
            return FALSE;
        }
        $this->load->model("tag_model");
        foreach ($tag as $t) {
            if ($this->tag_model->get($t) == FALSE){
                $this->form_validation->set_message('check_tag', 'The Tag field is invalid');
                return FALSE;
            }
        }
        return TRUE;
    }

    public function check_collection($collection)
    {
        $this->load->model("collection_model");
        if ($this->collection_model->get($collection) == FALSE){
            $this->form_validation->set_message('check_collection', 'The Collection field is invalid');
            return FALSE;
        }
        return TRUE;
    }

    private function do_upload()
    {
        $directory_path = './public/images/images/';
        $config['upload_path'] = $directory_path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 10000;
        $config['max_width'] = 4000;
        $config['max_height'] = 3000;
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('upload_image')) {
            $error = $this->upload->display_errors();

            return ['result' => FALSE, 'error' => $error];
        } else {
            //$data = array('upload_data' => $this->upload->data());
            return ['result' => TRUE, 'data' => substr($config['upload_path'], 2) . $this->upload->data()['file_name'],];
        }
    }
}