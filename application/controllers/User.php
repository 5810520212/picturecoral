<?php
/**
 * Created by PhpStorm.
 * UserModel: Wassana-lerdna
 * Date: 22/12/2561
 * Time: 14:00
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();


    }

    public function index()
    {
        check_login();

        $this->show('images');
    }

    public function EditProfile(){

        check_login();

        $this->load->library('form_validation');

        $this->load->model('User_model');
        $user = $this->User_model->get_user_by_name($this->session->set_session_name);

        if ($this->input->post('name', TRUE) != $user['name']) {
            $is_unique = '|is_unique[user.name]';
        } else {
            $is_unique = '';
        }

        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[20]'. $is_unique);
        $this->form_validation->set_rules('password', 'Password', 'trim|matches[passwordconf]|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('passwordconf', 'Password Confirmation', 'trim');

        $this->load->library('user_agent');
        $referrer =  $this->agent->referrer();
        if($this->form_validation->run() == FALSE){

            $this->session->set_flashdata('popup_show', 'popup_edit');
            $this->session->set_flashdata('error_edit', validation_errors('<div class="error">', '</div>'));
            $this->session->set_flashdata('name', $this->input->post('name', TRUE));

        } else {
            $do_upload = $this->do_upload();

            $submit_data['name'] = $this->input->post('name', TRUE);
            $submit_data['email'] = $user['email'];
            $password = $this->input->post('password', TRUE);
            if ($password == "") {
                $submit_data['password'] = $user['password'];
            } else {
                $submit_data['password'] = password_hash($password, PASSWORD_BCRYPT);
            }
            if ($do_upload['result'] === FALSE) {
                $submit_data['path_image'] = $user['path_image'];
            } else {
                $submit_data['path_image'] = $do_upload['data'];
            }

            if ($this->User_model->update_user($user['id'], $submit_data)) {
                $this->session->set_flashdata('notify_message', 'Update user successfully.');
                $this->session->set_userdata('set_session_name', $submit_data['name']);
            } else {
                $this->session->set_flashdata('notify_message', 'Oh! There is a problem.');
            }
        }
        redirect($referrer, 'refresh');
    }

    public function show($filter = 'images') {

        check_login();

        $arr_validation = [
            'images' => 'images',
            'favorite' => 'favorite',
        ];

        if (!array_key_exists($filter, $arr_validation)) {
            $filter = $arr_validation[0];
        }

        $this->load->model('user_model');
        $this->load->model('image_model');
        $this->load->model('favorite_model');

        $this->load->library('pagination');

        $limit_per_page = 16;
        $start_index = $this->uri->segment(4) ? $this->uri->segment(4) : 0 ;
        $user_id = $this->user_model->get_user_by_name($this->session->set_session_name)['id'];
        if ($filter == 'images') {
            $total_records = $this->image_model->count_image(['id_user' => $user_id]);
        } elseif ($filter == 'favorite') {
            $total_records = $this->favorite_model->count_favorite(['id_user' => $user_id]);
        }

        $data['id_user'] = $user_id;
        $data['images'] = array();
        $data['pagination_link'] = "";

        if ($total_records > 0) {
            $is_favorite_arr = [];

            if ($filter == 'images') {
                $data['images'] = $this->image_model->show_post_pagination($limit_per_page, $start_index, ['id_user' => $user_id]);

                if ($data['images'] === FALSE) {
                    $data['images'] = [];
                }

                foreach ($data['images'] as $image) {
                    $is_favorite_arr[] = $this->favorite_model->get($image['id'], $user_id);
                }
            } else {
                $is_favorite_arr = $this->favorite_model->get_by_iduser($user_id);
                $id_array = array();
                foreach ($is_favorite_arr as $f) {
                    $id_array[] = $f['id_image'];
                }
                $data['images'] = $this->image_model->show_image_pagination($limit_per_page, $start_index, $id_array);

                if ($data['images'] === FALSE) {
                    $data['images'] = [];
                }

            }

            $data['is_favorite_arr'] = $is_favorite_arr;
            $config['base_url'] = base_url('/user/show/'. $filter . '/');
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $this->pagination->initialize($config);
            $data['pagination_link'] = $this->pagination->create_links();
        }

        $data['page'] = "user";
        $this->load->view('header', $data);
        $this->load->view('content_user');
    }

    private function do_upload()
    {
        $directory_path = './public/images/user/';
        $config['upload_path'] = $directory_path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 10000;
        $config['max_width'] = 4000;
        $config['max_height'] = 3000;
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('upload_edit_image_profile')) {
            $error = $this->upload->display_errors();

            return ['result' => FALSE, 'error' => $error];
        } else {
            return ['result' => TRUE, 'data' => substr($config['upload_path'], 2) . $this->upload->data()['file_name'],];
        }
    }
}