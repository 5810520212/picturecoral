<?php
/**
 * Created by PhpStorm.
 * User: Wassana-lerdna
 * Date: 27/12/2561
 * Time: 16:58
 */

if ( ! function_exists('check_login'))
{
    function check_login() {
        $CI = get_instance();
        if (empty($CI->session->set_session_name)) {
            redirect('LogIn');
        }
    }
}

if ( ! function_exists('check_guest'))
{
    function check_guest() {
        $CI = get_instance();
        if ($CI->session->set_session_name) {
            redirect('Main');
        }
    }
}