<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection_model extends CI_Model
{
    public function get($name_collection)
    {
        $this->db->where('name', $name_collection);
        $query = $this->db->get('collections');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function get_bylike($name_collection)
    {
        $this->db->like('name', $name_collection);
        $query = $this->db->get('collections');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function count_bylike($name_collection)
    {
        $this->db->like('name', $name_collection);
        return $this->db->count_all_results('image');
    }


}