<?php
/**
 * Created by PhpStorm.
 * User: Wassana-lerdna
 * Date: 27/12/2561
 * Time: 2:12
 */

class Favorite_model extends CI_Model
{

    public function insert_data($data)
    {
        return $this->db->insert('favorite', $data);
    }

    public function get_by_iduser($id_user) {
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('favorite');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function get($id_image, $id_user) {
        $this->db->where('id_image', $id_image);
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('favorite');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function delete($id_image, $id_user)
    {
        $this->db->where('id_image', $id_image);
        $this->db->where('id_user', $id_user);
        return $this->db->delete('favorite');
    }

    public function count_favorite ($filter_array = array())
    {

        foreach ($filter_array as $key => $value) {
            $this->db->where($key, $value);
        }

        return $this->db->count_all_results('favorite');
    }
}