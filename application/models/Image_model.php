<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_model extends CI_Model
{
    public function insert_data($data)
    {
        $status = $this->db->insert('image', $data);
        if (!$status) {
            return FALSE;
        }
        return $this->db->insert_id();
    }

    public function get_images ()
    {
        $query = $this->db->get('image');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_image($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('image');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function get_image_by_iduser($id_user)
    {
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('image');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function count_image($filter_array = array(), $operation = 'and')
    {
        foreach ($filter_array as $key => $value) {
            if ($operation == 'and') {
                $this->db->where($key, $value);
            } elseif ($operation == 'or') {
                $this->db->or_where($key, $value);
            }
        }
        return $this->db->count_all_results('image');
    }

    public function count_image_bylike($filter_array = array(), $operation = 'and')
    {
        foreach ($filter_array as $key => $value) {
            if ($operation == 'and') {
                $this->db->like($key, $value);
            } elseif ($operation == 'or') {
                $this->db->or_like($key, $value);
            }
        }

        return $this->db->count_all_results('image');
    }

    public function update_image($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('image', $data);
    }

    public function delete_image($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('image');
    }

    public function show_post_pagination($limit, $start, $filter_array = array(), $operation = "and")
    {

        foreach ($filter_array as $key => $value) {
            if ($operation == "and") {
                $this->db->where($key, $value);
            } elseif ($operation == "or") {
                $this->db->or_where($key, $value);
            }
        }

        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('image');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function show_image_pagination($limit, $start, $id_array = array())
    {
        foreach ($id_array as $id) {
            $this->db->or_where("id", $id);
        }

        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('image');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function show_search_image_pagination($limit, $start, $conllections = array(), $id_array = array())
    {
        foreach ($id_array as $id) {
            $this->db->or_where("id", $id);
        }

        foreach ($conllections as $conllection) {
            $this->db->or_where("id_collection", $conllection['id']);
        }

        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get('image');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function count_search_image_pagination($conllections = array(), $id_array = array())
    {
        foreach ($id_array as $id) {
            $this->db->or_where("id", $id);
        }

        foreach ($conllections as $conllection) {
            $this->db->or_where("id_collection", $conllection['id']);
        }

        return $this->db->count_all_results('image');
    }


}