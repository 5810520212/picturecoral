<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_tag_model extends CI_Model
{
    public function insert_data($data)
    {
        return $this->db->insert('image_tag', $data);
    }

    public function get_by_tag($id_tag)
    {
        $this->db->where('id_tag', $id_tag);
        $query = $this->db->get('image_tag');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;

    }

    public function count_image($filter_array = array(), $operation = 'and')
    {
        foreach ($filter_array as $key => $value) {
            if ($operation == 'and') {
                $this->db->where($key, $value);
            } elseif ($operation == 'or') {
                $this->db->or_where($key, $value);
            }
        }
        return $this->db->count_all_results('image_tag');
    }

    public function get_by_image($id_image)
    {
        $this->db->where('id_image', $id_image);
        $query = $this->db->get('image_tag');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }

    public function delete_by_image($id_image)
    {
        $this->db->where('id_image', $id_image);
        return $this->db->delete('image_tag');
    }

}