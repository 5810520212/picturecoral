<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_model extends CI_Model
{
    public function get($name_tag)
    {
        $this->db->where('name', $name_tag);
        $query = $this->db->get('tag');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function get_tag_by_idtag ($id_tag)
    {
        $this->db->where('id', $id_tag);
        $query = $this->db->get('tag');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function get_bylike($name_tag)
    {
        $this->db->like('name', $name_tag);
        $query = $this->db->get('tag');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $item) {
                $data[] = $item;
            }
            return $data;
        }
        return FALSE;
    }


}