<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function insert_data($data)
    {
        return $this->db->insert('user', $data);
    }

    public function get_user($user_id)
    {
        $this->db->where('id', $user_id);
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function get_user_by_name($user_name)
    {
        $this->db->where('name', $user_name);
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        }
        return FALSE;
    }

    public function update_user($user_id, $data)
    {
        $this->db->where('id', $user_id);
        return $this->db->update('user', $data);
    }

    public function delete_user($user_id)
    {
        $this->db->where('id', $user_id);
        return $this->db->delete('user');
    }

    public function login($email, $password)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $hash = $row->password;
            if (password_verify($password, $hash)) {
                return $row->name;
            }
        }
        return FALSE;
    }
}