<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="UTF-8">

    <title>ABOUT CREATOR</title>

    <link rel="stylesheet" href="<?= base_url() ?>public/css/about.css">
    <link rel="stylesheet" href="<?= base_url() ?>public/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Caveat+Brush|Sacramento" rel="stylesheet">

</head>
<body>

<div class="container-about font1 color-font-livingcoral color-bg-softpink">
    <div class="site-header-about">
        <h1 class="center" style="margin: 20px">ALL ABOUT CREATOR</h1>
    </div>

    <div class="site-info-about margin-0">
        <div class="site-intro-image">
            <div style="background: url(<?= base_url('public/images/about/9.jpg') ?>) center;background-size: cover;"
                 class="intro-image"></div>
        </div>
        <div class="site-intro-info intro-info">
            <p>wassana lerdna (poysian), 5810520212</p>
            <p>electrical and computer engineering department</p>
            <p>thammasat school of engineering</p>
            <p>TU81 x CN14 x ENTUgear26</p>

        </div>
    </div>

    <div class="site-image-about">
        <div class="site-image1">
<!--            <img src=" --><?//= base_url('public/images/about/1.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/1.jpg') ?>) center;
                    background-size: cover;"></div>
        </div>
        <div class="site-image2">
<!--            <img src=" --><?//= base_url('public/images/about/2.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/2.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image3">
<!--            <img src=" --><?//= base_url('public/images/about/3.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/3.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image4">
<!--            <img src=" --><?//= base_url('public/images/about/4.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/12.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image5">
<!--            <img src=" --><?//= base_url('public/images/about/5.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/5.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image6">
<!--            <img src=" --><?//= base_url('public/images/about/6.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/6.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image7">
<!--            <img src=" --><?//= base_url('public/images/about/7.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/7.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image8">
<!--            <img src=" --><?//= base_url('public/images/about/8.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/8.jpg') ?>) center;background-size: cover;"></div>
        </div>
        <div class="site-image9">
<!--            <img src=" --><?//= base_url('public/images/about/13.jpg') ?><!-- " class="show-image-about">-->
            <div class="show-image-about" style="background: url(<?= base_url('public/images/about/10.jpg') ?>) center;background-size: cover;"></div>
        </div>
    </div>


</div>


</body>
</html>
