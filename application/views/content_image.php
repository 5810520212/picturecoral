<?php
$theme_bg = "theme2_bg";
$theme_font = "theme2_font";
?>

<main class="site-main <?= $theme_bg ?>">
    <div class="site-main-show-image">
        <div class="site-user">
            <?php $name_user = $owner['name'] ?>
            <ul>
                <li><img src="<?= base_url($owner['path_image']) ?>"
                         class="user-header-image space-left-button-header"></li>
                <li><h2 class="font1 margin-0 <?= $theme_font ?>"><?= $name_user ?></h2></a></li>
            </ul>
        </div>
        <div class="site-show-image">
<!--            <img src="--><?//= base_url($image['path_image']) ?><!--" class="size-center-image" width="100%" height="100%">-->
            <div style="background-image: url(<?= base_url($image['path_image']) ?>); background-size: contain;background-repeat: no-repeat;
                    background-position: center;
                    width: 100%;height: 100%;"
                 class="size-center-image">
            </div>
        </div>
        <div class="site-btn-image">
            <a href="<?= base_url('image/download/'. $image['id']) ?>" class="font1 <?= $theme_font ?> show-btn-image">
                <img src="<?= base_url("/public/images/054-down-arrow.png") ?>" class="size-icon"> &nbsp; Download</a><br><br>
            <a href="<?= base_url('image/favorite_submit/'. $image['id']) ?>" class="font1 <?= $theme_font ?> show-btn-image">
                <?php if ($is_favorite) : ?>
                    <img src="<?= base_url("/public/images/028-heart-2.png") ?>" class="size-icon">
                <?php else : ?>
                    <img src="<?= base_url("/public/images/028-heart-1.png") ?>" class="size-icon">
                <?php endif; ?>
                &nbsp; Favorite
            </a> <br><br>
            <a href="#" class="font1 <?= $theme_font ?> show-btn-image" onclick="div_show('popup_share')">
                <img src="<?= base_url("/public/images/058-file-1.png") ?>" class="size-icon"> &nbsp; Share
            </a> <br><br>

        </div>
        <div class="site-show-tag">
            <?php
            $name_tag = "";
                for ($i = 0; $i < count($tag); $i++) {
                    if ($i == count($tag) - 1){
                        $name_tag = $name_tag . $tag[$i];
                    } else {
                        $name_tag = $name_tag . $tag[$i] . ", &nbsp;";
                    }

                }
            ?>
            <h2 class="font1 <?= $theme_font ?> show-tag">Tag : <?= $name_tag ?></h2>
        </div>
    </div>

    <div id="popup_share">
        <div id="popup_contact_share">
            <button type="button" onclick="div_exit('popup_share')" class="button-exit-popup">X</button>
            <div class="share_container">
                <div class="share_container_facebook">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url("image/show_image/". $image['id']) ?>" target="_blank">
                        <img src="<?= base_url('public/images/facebook.png') ?>" width="50px" height="50px">
                    </a>
                </div>
                <div class="share_container_twitter">
                    <a class="twitter-share-button"
                       href="https://twitter.com/intent/tweet?text=<?= base_url("image/show_image/". $image['id']) ?>"
                       data-size="large">
                        <img src="<?= base_url('public/images/twitter.png') ?>" width="50px" height="50px">
                    </a>
                </div>
                <div class="share_container_email">
                    <a href="mailto:?subject=I wanted you to see this image&amp;body=Check out this site <?= base_url("image/show_image/". $image['id']) ?>."
                       title="Share by Email">
                        <img src="<?= base_url('public/images/gmail.png') ?>" width="50px" height="50px">
                    </a>
                </div>
            </div>
        </div>
    </div>

</main>
</div>
</body>
</html>