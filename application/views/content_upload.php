<?php
$theme_bg = "theme2_bg";
$theme_font = "theme2_font";
?>


<main class="site-main site-main-upload <?= $theme_bg ?>">
    <div class="site-image">
        <img src="" style="display: none;" height="200" width="500" id="image" class="upload-image">
    </div>

    <div class="site-upload">
        <div class="site-choose-image">
            <?php
            $error = $this->session->flashdata('error_upload');
            if (isset($error)) {
                echo $error . "<br>";
            }

            $error = $this->session->flashdata('error_formupload');
            if (isset($error)) {
                echo $error . "<br>";
            }
            ?>
            <?php
            $attributes = array('class' => '', 'id' => '');
            echo form_open_multipart('Upload/after_upload', $attributes);
            ?>
            <div class="form-group">
                <?php
                $data = array(
                    'value' => 'choose image',
                    'onclick' => "onclickUpload('upload_image');",
                    'class' => 'button-upload color-font-softpink',
                    'type' => 'button'
                );
                echo form_input($data);
                echo "<span id=\"image_file_name\">No image selected</span>";
                $data = array(
                    'name' => 'upload_image',
                    'id' => 'upload_image',
                    'onchange' => "onchangeUpload('upload_image', 'image_file_name', 'image')",
                    'style' => 'display: none;',
                    'accept' => 'image/*'
                );
                echo form_upload($data) . '<br>';
                ?>
            </div>

            <div class="site-tag">
                <?php
                $tag = array("green", "nature", "art", "city", "travel", "fun", "cooking", "animal", "transporation",
                    "sky", "sea", "landscape", "house", "classic", "street", "road", "shop", "food", "music", "lake");
                echo "<h2 class='font1 <?= $theme_font ?> upload-tag'>Choose Tags for your image</h2>";
                ?>

                <?php for ($i = 1 ; $i <= 20 ; $i++) { ?>
                    <div class="site-tag<?= $i ?>">
                        <?php
                            $data = array(
                                'name' => 'tag[]',
                                'id' => 'tag'.$i,
                                'value' => $tag[$i-1],
                                'type' => 'checkbox',
                            );
                            echo form_input($data) . "&nbsp;";
                            echo form_label($tag[$i-1].' &nbsp;', 'tag'.$i, "class=\"font1 $theme_font size-font-checkbox\"");
                        ?>
                    </div>
                <?php } ?>
            </div>

            <div class="site-collection">
                <?php
                echo form_label('Collections &nbsp;', 'collections', "class=\"font1 $theme_font size-font-dropdown\"");

                $options = array(
                    'All' => 'All',
                    'Fashion' => 'Fashion',
                    'Animals' => 'Animals',
                    'Business' => 'Business',
                    'Flowers' => 'Flowers',
                    'People' => 'People',
                    'Art' => 'Art',
                    'Foods' => 'Foods',
                );
                echo form_dropdown('collections', $options);
                ?>
            </div>

            <div class="site-form-upload">
                <div style="padding-left: 25%">
                    <?php
                    $data = array(
                        'class' => 'button-upload color-font-softpink',
                    );
                    echo "<br>";
                    echo form_submit('upload', 'upload', $data);
                    echo form_close();
                    ?>
                </div>
            </div>
        </div>

    </div>


</main>
</div>
</body>
</html>