<?php
$theme_bg = "theme2_bg";
$theme_font = "theme2_font";
?>

<main class="site-main <?= $theme_bg ?>">
    <div class="show-image-group">
        <?php
        $limit_per_row = 4;
        $current_col = 0;
        $max_loop = count($images) % $limit_per_row == 0 ? count($images) : count($images) + ($limit_per_row - (count($images) % $limit_per_row)) ;
        ?>

        <?php for ($i = 0 ; $i < $max_loop ; $i++) : ?>
            <?php
            $item = $i < count($images) ? $images[$i] : FALSE ;
            ?>
            <?php if ($current_col % $limit_per_row == 0) : ?>
                <div class="show_image_row">
            <?php endif; ?>

            <div class="show-image<?= $current_col ?> show-image"id="image<?= $item['id'] ?>"
                 onmouseover="show_menu_image('image<?= $item['id'] ?>_menu');"
                 onmouseout="hide_menu_image('image<?= $item['id'] ?>_menu');"
            >
                <?php if ($item !== FALSE) : ?>
                    <a href="<?= base_url("image/show_image/" . $item['id']) ?>" ><img src="<?= base_url("/" . $item['path_image']) ?>" width="100%" height="100%"></a>
                <?php endif; ?>

                <?php if ($item !== FALSE) : ?>
                    <div class="show-image-menu-user" id="image<?= $item['id'] ?>_menu">
                        <div class="show-image-menu-share-user show-image-menu-icon">
                            <a href="#" onclick="share_show('popup_share', '<?= base_url('image/show_image/'. $item['id']) ?>');">
                                <img src="<?= base_url('public/images/058-file-1.png') ?>" width="30px" height="30px">
                            </a>
                        </div>
                        <div class="show-image-menu-favor-user show-image-menu-icon">
                            <a href="<?= base_url('image/favorite_submit/'. $item['id']) ?>">
                                <?php
                                $is_favorite = array_key_exists($i, $is_favorite_arr) ? $is_favorite_arr[$i] : FALSE;
                                ?>
                                <?php if ($is_favorite) : ?>
                                    <img src="<?= base_url('public/images/028-heart-2.png') ?>" width="30px" height="30px">
                                <?php else : ?>
                                    <img src="<?= base_url('public/images/028-heart-1.png') ?>" width="30px" height="30px">
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="show-image-menu-download-user show-image-menu-icon">
                            <a href="<?= base_url('image/download/'. $item['id']) ?>">
                                <img src="<?= base_url('public/images/054-down-arrow.png') ?>" width="30px" height="30px">
                            </a>
                        </div>
                        <div class="show-image-menu-delete-user show-image-menu-icon">
                            <?php if ($item['id_user'] == $id_user) : ?>
                                <a href="<?= base_url('image/delete_submit/'. $item['id']) ?>"  onclick="return confirm('Are you sure?');">
                                    <img src="<?= base_url('public/images/066-delete.png') ?>" width="30px" height="30px"></a>
                            <?php else: ?>
                                <img src="<?= base_url('public/images/066-delete-1.png') ?>" width="30px" height="30px">
                            <?php endif; ?>

                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <?php if ($current_col % $limit_per_row == $limit_per_row - 1) : ?>
                </div>
            <?php endif; ?>

            <?php
            $current_col = ($current_col + 1) % $limit_per_row;
            ?>
        <?php endfor; ?>
    </div>

    <div class="pagination">
        <!--<a href="#">Older »</a>-->
        <?= $pagination_link; ?>
    </div>

    <div id="popup_share">
        <div id="popup_contact_share">
            <button type="button" onclick="div_exit('popup_share')" class="button-exit-popup">X</button>
            <div class="share_container">
                <div class="share_container_facebook">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=example.org" target="_blank" id="share_facebook">
                        <img src="<?= base_url('public/images/facebook.png') ?>" width="50px" height="50px">
                    </a>
                </div>
                <div class="share_container_twitter">
                    <a class="twitter-share-button"
                       href="https://twitter.com/intent/tweet?text=Hello%20world"
                       data-size="large" id="share_twitter">
                        <img src="<?= base_url('public/images/twitter.png') ?>" width="50px" height="50px">
                    </a>
                </div>
                <div class="share_container_email">
                    <a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site http://www.website.com."
                       title="Share by Email" id="share_email">
                        <img src="<?= base_url('public/images/gmail.png') ?>" width="50px" height="50px">
                    </a>
                </div>
            </div>
        </div>
    </div>

</main>
</div>
</body>
</html>