<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="UTF-8">

    <title>Picture Coral</title>

    <link rel="stylesheet" href="<?= base_url() ?>public/css/bootstrap-form.css">
    <link rel="stylesheet" href="<?= base_url() ?>public/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Caveat+Brush|Sacramento" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public/css/flaticon/flaticon.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type="text/javascript" src="<?= base_url("public/js/js.js") ?>"></script>

</head>

<?php
$CI = get_instance();
$CI->load->model('User_model');
$user = $CI->User_model->get_user_by_name($this->session->set_session_name);

$load = "";
$popup_show = $this->session->flashdata('popup_show');
if (isset($popup_show)) {
    $load = "onload=\"div_show('" . $popup_show . "')\"";
}

$theme_bg = "theme2_bg";
$theme_font = "theme2_font";

if ($page == "main" || $page == 'search') {
    $theme_bg = "theme1_bg";
    $theme_font = "theme1_font";
}
?>

<body <?= $load ?>>

<div class="container">
    <header class="site-header <?= $theme_bg . " " . $theme_font ?>">
        <div class="site-logo">
            <?php if ($page == "user") : ?>
                <h2><a href="<?= base_url("Upload"); ?>" class="<?= $theme_font ?> font1"><i
                                class="flaticon-037-outbox"></i> Upload image</a></h2>
            <?php elseif ($page == "upload" || $page == "image") : ?>
                <a href="<?= base_url("/") ?>"><img src="<?= base_url('public/images/logo3-1.png') ?>" width="190"
                                                    height="35" class="magin_images"></a>
            <?php else : ?>
                <a href="<?= base_url("/") ?>"><img src="<?= base_url('public/images/logo3.png') ?>" width="190"
                                                    height="35" class="magin_images"></a>
            <?php endif; ?>
        </div>

        <div class="site-each-page">
            <?php if ($page == "main" || $page == 'search') : ?>
                <div class="search-container">
                    <?php
                    $attributes = array('class' => '', 'id' => '', 'method' => 'get');
                    echo form_open('Search', $attributes);
                    ?>
                    <?php
                    $data = array(
                        'name' => 'search',
                        'id' => 'search',
                        'value' => $this->session->flashdata('search'),
                        'placeholder' => 'Search image',
                        'class' => 'search-block',
                    );
                    echo form_input($data);
                    $data = array(
                        'class' => 'search-btn color-font-livingcoral color-bg-softpink',
                        'type' => 'submit',
                    );
                    echo form_button($data, '<i class="fa fa-search"></i>');
                    echo form_close();
                    ?>
                </div>
            <?php elseif ($page == "about") : ?>
                <p class="font">ALL ABOUT CREATOR</p>
            <?php elseif ($page == "upload") : ?>
                <h1 class="font center margin-0 your-user">UPLOAD IMAGE</h1>
            <?php elseif ($page == "image") : ?>

            <?php elseif ($page == "user") : ?>
                <h1 class="font center margin-0 your-user">Your</h1>
                <div class="center">
                    <ul class="margin-0 ">
                        <li>
                            <a href="<?= base_url("user/show/images"); ?>" class="button-user font1 <?= $theme_font ?>">
                                <h2>images</h2></a>
                        </li>
                        <li><h2>|</h2></li>
                        <li>
                            <a href="<?= base_url("user/show/favorite"); ?>"
                               class="button-user font1 <?= $theme_font ?>"><h2>favorite</h2></a>
                        </li>
                    </ul>
                </div>


            <?php endif; ?>
        </div>


        <div class="site-button">
            <?php if ($name_user = $this->session->set_session_name) : ?>
                <ul class="header-right ul-button-header">
                    <li><a href="<?= base_url("/user") ?>"><h2
                                    class="font1 margin-0 <?= $theme_font ?>"><?= $name_user ?></h2></a></li>
                    <li><a href="<?= base_url("/user") ?>"><img src="<?= base_url($user['path_image']) ?>"
                                                                class="user-header-image space-left-button-header"></a>
                    </li>
                    <li>
                        <div class="dropdown-list center">
                            <button class="dropbtn-list font1 <?= $theme_font ?>"><i class="flaticon-027-list"></i>
                            </button>
                            <div class="dropdown-content-list font1">
                                <a href="#" class="font1 <?= $theme_font ?>" onclick="div_show('popup_edit')">edit
                                    profile</a>
                                <a href="<?= base_url("/logout") ?>">log out</a>
                                <svg height="2" width="100">
                                    <line x1="0" y1="0" x2="200" y2="0" style="stroke: #fa7169;stroke-width: 2px;"/>
                                </svg>
                                <a href="<?= base_url("/main") ?>">home</a>
                                <a href="<?= base_url("/about") ?>">about</a>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php else: ?>
                <ul class="header-right ul-button-header">
                    <li><a href="#" class="font1 <?= $theme_font ?>" onclick="div_show('popup_login')">Log In</a></li>
                    <li class="space-left-button-header"><a href="#" class="font1 <?= $theme_font ?>"
                                                            onclick="div_show('popup_signup')">Join Us!</a></li>
                    <li>
                        <div class="dropdown-list center">
                            <button class="dropbtn-list font1 color-font-softpink"><i class="flaticon-027-list"></i>
                            </button>
                            <div class="dropdown-content-list font1">
                                <a href="<?= base_url("/about") ?>">about</a>
                            </div>
                        </div>
                    </li>
                </ul>


            <?php endif; ?>
        </div>


    </header>


    <div id="popup_signup">
        <div id="popup_contact">
            <h1 class="font1">Join us!</h1>
            <h2 class="font1">Create your account</h2>
            <?php
            $error = $this->session->flashdata('error');
            if (isset($error)) {
                echo $error . "<br>";
            }
            ?>
            <button type="button" onclick="div_exit('popup_signup')" class="button-exit-popup">X</button>
            <?php
            $attributes = array('class' => '', 'id' => '');
            echo form_open('JoinUs', $attributes);
            ?>

            <div class="form-group">
                <?php
                $data = array(
                    'name' => 'name',
                    'id' => 'name',
                    'value' => $this->session->flashdata('name'),
                    'maxlength' => '20',
                    'placeholder' => 'Enter your name',
                    'class' => 'form-control popup_control'
                );
                echo form_input($data) . "<br>";

                $data = array(
                    'name' => 'email',
                    'id' => 'email',
                    'type' => 'email',
                    'value' => $this->session->flashdata('email'),
                    'placeholder' => 'Enter your email',
                    'class' => 'form-control popup_control'
                );
                echo form_input($data) . "<br>";

                $data = array(
                    'name' => 'password',
                    'id' => 'password',
                    'maxlength' => '20',
                    'placeholder' => 'Enter your password',
                    'class' => 'form-control popup_control'
                );
                echo form_password($data) . "<br>";

                $data = array(
                    'name' => 'passwordconf',
                    'id' => 'passwordconf',
                    'maxlength' => '20',
                    'placeholder' => 'Repeat your password',
                    'class' => 'form-control popup_control'
                );
                echo form_password($data) . "<br>";
                ?>

            </div>

            <?php

            $data = array(
                'class' => 'button-join',
            );
            echo form_submit('join us', 'join us', $data);
            echo form_close();
            ?>

        </div>
    </div>

    <div id="popup_login">
        <div id="popup_contact_login">
            <h1 class="font1">Log in</h1>
            <?php
            $error_login = $this->session->flashdata('error_login');
            if (isset($error_login)) {
                echo $error_login . "<br>";
            } ?>
            <button type="button" onclick="div_exit('popup_login')" class="button-exit-popup">X</button>
            <?php
            $attributes = array('class' => '', 'id' => '');
            echo form_open('LogIn', $attributes);
            ?>
            <div class="form-group">
                <?php

                $data = array(
                    'name' => 'email',
                    'id' => 'email',
                    'type' => 'email',
                    'value' => $this->session->flashdata('email'),
                    'placeholder' => 'Enter your email',
                    'class' => 'form-control popup_control'
                );
                echo form_input($data) . "<br>";

                $data = array(
                    'name' => 'password',
                    'id' => 'password',
                    'maxlength' => '20',
                    'placeholder' => 'Enter your password',
                    'class' => 'form-control popup_control'
                );
                echo form_password($data) . "<br>";
                ?>

            </div>

            <?php

            $data = array(
                'class' => 'button-login',
            );
            echo form_submit('log in', 'log in', $data);
            echo form_close();
            ?>

        </div>
    </div>

    <?php if ($name_user = $this->session->set_session_name) : ?>
        <div id="popup_edit">
            <div id="popup_contact_edit">
                <h1 class="font1">Edit profile</h1>
                <?php
                $error_login = $this->session->flashdata('error_edit');
                if (isset($error_login)) {
                    echo $error_login . "<br>";
                } ?>
                <button type="button" onclick="div_exit('popup_edit')" class="button-exit-popup">X</button>
                <?php
                $attributes = array('class' => '', 'id' => '');
                echo form_open_multipart('user/EditProfile', $attributes);

                $user_path = $user['path_image'];
                ?>
                <div onclick="onclickUpload('upload_edit_image_profile');">
                    <img src="<?= base_url($user['path_image']) ?>"
                         class="edit-image" id="image_edit_profile">
                    <?php
                    $data = array(
                        'name' => 'upload_edit_image_profile',
                        'id' => 'upload_edit_image_profile',
                        'onchange' => "showImage('upload_edit_image_profile', 'image_edit_profile', true)",
                        'style' => 'display: none;',
                        'accept' => 'image/*'
                    );
                    echo form_upload($data) . '<br>';
                    ?>
                </div>
                <div class="form-group">
                    <?php

                    $data = array(
                        'name' => 'name',
                        'id' => 'name',
                        'maxlength' => '20',
                        'value' => $user['name'],
                        'placeholder' => 'Enter your email',
                        'class' => 'form-control popup_control'
                    );
                    echo form_input($data) . "<br>";

                    $data = array(
                        'name' => 'password',
                        'id' => 'password',
                        'maxlength' => '20',
                        'placeholder' => 'Enter your password',
                        'class' => 'form-control popup_control'
                    );
                    echo form_password($data) . "<br>";

                    $data = array(
                        'name' => 'passwordconf',
                        'id' => 'passwordconf',
                        'maxlength' => '20',
                        'placeholder' => 'Repeat your password',
                        'class' => 'form-control popup_control'
                    );
                    echo form_password($data) . "<br>";

                    ?>

                </div>

                <?php

                $data = array(
                    'class' => 'button-edit',
                );
                echo form_submit('Edit profile', 'Edit profile', $data);
                echo form_close();
                ?>

            </div>
        </div>
    <?php endif; ?>





