
function div_show(id) {
    document.getElementById(id).style.display = "block";
}

function div_exit(id) {
    document.getElementById(id).style.display = "none";
}

function share_show(id, url) {
    document.getElementById(id).style.display = "block";
    document.getElementById("share_facebook").href = "https://www.facebook.com/sharer/sharer.php?u=" + url;
    document.getElementById("share_twitter").href = "https://twitter.com/intent/tweet?text=" + url;
    document.getElementById("share_email").href = 'mailto:?subject=I wanted you to see this image&body=Check out ' +
        'this site ' + url  + '.';
}


function showImage(id, image_show_id, exist_image = false) {
    var input_file = document.getElementById(id);
    if (input_file.files && input_file.files[0]) {
        var image = document.getElementById(image_show_id);
        image.src = URL.createObjectURL(input_file.files[0]);
        if (!exist_image) {
            image.style.display = "block";
        }
    }
}

function onclickUpload(id) {
    var input_file = document.getElementById(id);
    input_file.click();
}

function onchangeUpload(id, text_id, image_show_id) {
    var input_file = document.getElementById(id);
    var file = input_file.files[0];
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if (!ValidImageTypes.includes(fileType)) {
        alert("File type invalid!");
        return;
    }
    showImage(id, image_show_id);

    var text = document.getElementById(text_id);
    text.innerHTML = input_file.files.item(0).name;
}

function show_menu_image(id) {
    document.getElementById(id).style.display = "grid";
}

function hide_menu_image(id) {
    document.getElementById(id).style.display = "none";
}